package cn.xielong.provider.service.impl;

import cn.xielong.provider.service.ProviderService;
import org.apache.dubbo.config.annotation.Service;

// 注解标记此类的方法暴露为dubbo接口
@Service
public class ProviderServiceImpl implements ProviderService {

    // dubbo接口实现内容
    public String service() {
        return "Provider invoke";
    }
}
