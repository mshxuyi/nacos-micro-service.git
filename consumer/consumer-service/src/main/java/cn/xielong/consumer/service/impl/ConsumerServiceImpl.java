package cn.xielong.consumer.service.impl;

import cn.xielong.consumer.service.ConsumerService;
import cn.xielong.provider.service.ProviderService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    @Reference
    ProviderService providerService;

    public String service() {
        String service = providerService.service();
        return "Consumer invoke | " + service;
    }
}
