package cn.xielong.web.controller;

import cn.xielong.consumer.service.ConsumerService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {

    // 注入service (基于dubbo协议)
    // 生成接口代理对象，通过代理对象进行远程调用
    @Reference
    ConsumerService consumerService;

    @GetMapping("/service")
    public String service() {
        // 远程调用
        String service = consumerService.service();
        return "test | " + service;
    }
}
